import { BeanStub } from "../../context/beanStub";
import { CellCtrl, ICellComp } from "./cellCtrl";
import { Beans } from "../beans";
import { ITooltipParams } from "../tooltipComponent";
export declare class CellTooltipFeature extends BeanStub {
    private readonly cellCtrl;
    private readonly column;
    private readonly rowNode;
    private readonly beans;
    private cellComp;
    private tooltip;
    private tooltipSanatised;
    private genericTooltipFeature;
    private browserTooltips;
    constructor(ctrl: CellCtrl, beans: Beans);
    setComp(comp: ICellComp): void;
    private setupTooltip;
    private updateTooltipText;
    private createTooltipFeatureIfNeeded;
    refreshToolTip(): void;
    private getToolTip;
    getTooltipParams(): ITooltipParams;
    private getTooltipText;
    destroy(): void;
}
