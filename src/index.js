import React from 'react';
import ReactDOM from 'react-dom';
import MainPage from './components/MainPage';
import './index.scss';
import {BrowserRouter as Router,  Redirect, Route, Link, Switch} from "react-router-dom";
import EditableTable from './components/EditableTable'
import EditExpenseEntry from './components/EditExpenseEntry';

import WeddingsDisplay from './components/WeddingsDisplay';
import Login from './components/Login';
import EncodeBase64FileUpload from './components/FileUpload';
import MessagingService from './components/MessagingService';



ReactDOM.render(
  <Router>
    <Switch>

      {/* <Route path="/weddingsDisplay">
      <WeddingsDisplay ></WeddingsDisplay>
      </Route>  */}
          <Route exact path="/editPost">
            <EditableTable></EditableTable>
          </Route>
          <Route exact path="/messagingService">
            <MessagingService></MessagingService>
          </Route>
          <Route exact path="/login">
            <Login></Login>
          </Route>
          <Route exact path="/fileUpload">
            <EncodeBase64FileUpload></EncodeBase64FileUpload>
          </Route>
          <Route exact path="/editEntry">
            <EditableTable></EditableTable>
          </Route>
          <Route exact path = "/editExpenseEntry">
            <EditExpenseEntry></EditExpenseEntry>
          </Route>
          <Route exact path = "/messagingService">
            <MessagingService></MessagingService>
          </Route>
          
          <Route path = "/">
            <Login></Login>
          </Route>
        </Switch>
      </Router>
  ,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals

