import React from "react";

const ReadOnlyRow = ({ wedding, handleEditClick, handleDeleteClick }) => {

  return (
    <tr>
      <td>{wedding.wedding_id}</td>
      <td>{wedding.wedding_name}</td>
      <td>{wedding.wedding_description}</td>
      <td>{wedding.venue_id}</td>
      <td>{wedding.bride}</td>
      <td>{wedding.groom}</td>
      <td>{wedding.start_time}</td>
      <td>{wedding.end_time}</td>
      <td>{wedding.budget_planned}</td>
      <td>{wedding.no_of_guests}</td>
      <td>
        <button
          type="button"
          onClick={(event) => handleEditClick(event, wedding)}
        >
          Edit
        </button>
        <button type="button" onClick={() => handleDeleteClick(wedding.wedding_id)}>
          Delete
        </button>
      </td>
    </tr>
  );
};

export default ReadOnlyRow;