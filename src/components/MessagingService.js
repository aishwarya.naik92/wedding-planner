import React,  {useState, useEffect} from "react";
import './messaging.scss';
import Dropdown from 'react-dropdown';


const axios = require('axios')

export default function MessagingService(props){
    const [senderValue ,setSenderValue]=useState('');
    const [receiverValue ,setReceiverValue]=useState('');
    const [message, setMessages] = useState([]);
    const [employees, setEmployees] = useState([]);
    const [sendMessage, setSendMessage] = useState([]);
    const [thisIsSender, setThisIsSender] = useState('');


    useEffect(async () => {
      const result1 = await axios.get(
        'http://wedding-planner-authorization-service.wedding-planner-324818.wl.r.appspot.com/employees',
      );
      
      setEmployees(result1.data);
    }, []);

    function setThisIsSenderValue(data)
    {
      let compareEmail = props.userEmail + "@wed.com";
      for(let eachData of data)
      {
        if(eachData.sender === compareEmail)
        {
          setThisIsSender(compareEmail);
          break;
        }
      }
    }


    
    useEffect(async () => { 
      const result2 = await axios.get(
        `https://wedding-planner-messaging-service-dot-wedding-planner-324818.wl.r.appspot.com/messages`,
      );
      setMessages(result2.data);
      setThisIsSenderValue(result2.data);
    }, []);

    debugger;

    function handleChangeSelectSender(e){
        setSenderValue(e.target.value);
    }
    
    function handleChangeSelectReceiver(e){
      setReceiverValue(e.target.value);
    }

    function handleInput(e){
      setSendMessage(e.target.value);
    }

    async function handleButtonClick(e){
      const date = new Date();
      const sendMessageObj = {
        message: sendMessage,
        sender: senderValue,
        receiver: receiverValue,
        time: date.toISOString()
      }

      const response = await axios.post('https://wedding-planner-messaging-service-dot-wedding-planner-324818.wl.r.appspot.com/messages', sendMessageObj)
      //handle error - TODO

      
    }
    
    const messagesPopulate = message === undefined?  "": message.map((key) => {
      debugger;
      if(thisIsSender === props.userEmail)
      { 
        return (<div class="message-orange">
            <div class="message-sender">Sender: {key.sender}</div>
            <p class="message-content"> Message: {key.message}</p>
            <div><p class = "message-content"> </p></div>
            <div class="message-timestamp-right"> Time: {key.time}</div>
            </div>
            );
      }
      else 
      {
        return (<div class="message-blue">
          <div class="message-sender">Sender: {key.sender}</div>
          <p class="message-content"> Message: {key.message}</p>
          <div><p class = "message-content"> </p></div>
          <div class="message-timestamp-right"> Time: {key.time}</div>
          </div>
            );
          }
        }
             
      );



      debugger;

    return(
        
        <div class="app-container">
            
            {messagesPopulate} 
            { employees ?<div>
                            <label for="id_sender">
                              Sender
                            </label>
                            <select name="selectList" id="id_sender" onChange={handleChangeSelectSender} >
                              {employees && employees.map((x)=> <option>{x.email}</option>)}
                           </select> 
                           <label for="id_receiver">
                              Receiver
                            </label>
                            <select name="selectList" id="id_receiver" onChange = {handleChangeSelectReceiver}>
                              {employees.map((x)=> <option>{x.email}</option>)}
                           </select> 
                        </div>: ""
            }

            
            

            
        <input type="text" id="message" onChange={handleInput} placeholder="Type message here"> 
        
        </input>
        <button onClick={handleButtonClick}> SEND</button>
    </div>
    );


}
