import React from "react";

const EditableRow = ({
  editFormData,
  handleEditFormChange,
  handleCancelClick,
}) => {
  return (
    <tr>
      <td>
        <input
          type="text"
          required="required"
          placeholder="Enter expense id"
          name="expense_id"
          value={editFormData.expense_id}
          onChange={handleEditFormChange}
        disabled ></input>
      </td>
      <td>
        <input
          type="text"
          required="required"
          placeholder="Enter reason"
          name="reason"
          value={editFormData.reason}
          onChange={handleEditFormChange}
         ></input>
      </td>
      <td>
        <input
          type="number"
          required="required"
          placeholder="Enter wedding id"
          name="wedding_id"
          value={editFormData.wedding_id}
          onChange={handleEditFormChange}
        ></input>
      </td>
      <td>
        <input
          type="number"
          required="required"
          placeholder="Enter amount"
          name="amount"
          value={editFormData.amount}
          onChange={handleEditFormChange}
        ></input>
      </td>     
      <td>
        <button type="submit">Save</button>
        <button type="button" onClick={handleCancelClick}>
          Cancel
        </button>
      </td>
    </tr>
  );
};

export default EditableRow;