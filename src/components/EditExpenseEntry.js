import React, {useState, useEffect, Fragment} from "react";
import ReadOnlyExpenseRow from "./ReadOnlyExpenseRow";
import EditableExpenseRow from "./EditableExpenseRow";

const axios = require('axios')

export default function EditExpenseEntry() {
    const [expenseData, setExpenseData]  = useState([]);
    const [editExpenseId, setEditExpenseId] = useState([]);

    useEffect(async () => {
      const result = await axios(
        'http://35.202.158.179:3001/expenses',
      );
      setExpenseData(result.data);
    }, []);

    const [addFormData, setAddFormData] = useState({
        expense_id: 0,
        reason: '',
        wedding_id: 0,
        amount: 0
    });

    const [editFormData, setEditFormData] = useState({
        expense_id: 0,
        reason: '',
        wedding_id: 0,
        amount: 0
      });


  const handleAddFormChange = (event) => {
      event.preventDefault();

      const fieldName  = event.target.getAttribute('name');
      const fieldValue = event.target.value;

      const newFormData = {...addFormData};
      newFormData[fieldName] = fieldValue;

      setAddFormData(newFormData);
  }

  const handleEditFormChange = (event) => {
    event.preventDefault();

    const fieldName = event.target.getAttribute("name");
    const fieldValue = event.target.value;

    const newFormData = { ...editFormData };
    newFormData[fieldName] = fieldValue;
    setEditFormData(newFormData);
  };

  const handleAddFormSubmit = async (event) => {
      event.preventDefault();

      const newExpense = {
          expense_id: addFormData.expense_id,
          reason: addFormData.reason,
          wedding_id: addFormData.wedding_id,
          amount: addFormData.amount
      };

      const newExpenseData = [...expenseData, newExpense];
      
      const resp = await axios.post('http://35.202.158.179:3001/expenses', newExpense);
      setExpenseData(newExpenseData);
      setEditExpenseId(newExpenseData[newExpenseData.length-1]);
  }

  

  const handleEditFormSubmit = async (event) => {
    event.preventDefault();

    const editedExpense = {
      expense_id: editExpenseId,
      reason: editFormData.reason,
      wedding_id: Number(editFormData.wedding_id),
      amount: Number(editFormData.amount)
    };

    const newExpenses = [...expenseData];
    const index = expenseData.findIndex((expense) => expense.expense_id === editExpenseId);
    newExpenses[index] = editedExpense;

    setExpenseData(newExpenses);

    const response = axios.put(`http://35.202.158.179:3001/expenses/${editedExpense.expense_id}`, editedExpense);
    //TODO - Handle error
    setEditExpenseId(null);
  };

  const handleEditClick = (event, expense) => {
    event.preventDefault();
    setEditExpenseId(expense.expense_id);

    const formValues = {
      expense_id: expense.expense_id,
      reason: expense.reason,
      wedding_id: Number(expense.wedding_id),
      amount: Number(expense.amount)
    };

    setEditFormData(formValues);
  };

  const handleCancelClick = () => {
    setEditExpenseId(null);
  };

  const handleDeleteClick = async (expense_id) => {
    const newExpenses = [...expenseData];
    const index = expenseData.findIndex((expense) => expense.expense_id === expense_id);
    newExpenses.splice(index, 1);
    setExpenseData(newExpenses);
    const response = await axios.delete(`http://35.202.158.179:3001/expenses/${expense_id}`);
    //TODO handle error
  };

  return (
    <div>
      <form onSubmit = {handleEditFormSubmit}>
      <table>
        <thead>
          <tr>
            <th>Expense Id </th>
            <th> Expense Reason</th>
            <th>Wedding Id</th>
            <th>Amount</th>
          </tr>
        </thead>
        <tbody>
          {expenseData.map((expense) => (
            <Fragment>
              {editExpenseId === expense.expense_id ? (
                <EditableExpenseRow
                  editFormData={editFormData}
                  handleEditFormChange={handleEditFormChange}
                  handleCancelClick={handleCancelClick}
                />
                ) : (
                  <ReadOnlyExpenseRow
                    expense={expense}
                    handleEditClick={handleEditClick}
                    handleDeleteClick={handleDeleteClick}
                  />
                )
              }
            </Fragment>
            ))
          }
        </tbody>
      </table>
      </form>
      <div>
        <form onSubmit={handleAddFormSubmit}>
          <h2> Add a wedding Row</h2>
          <input type="text" name = "reason" placeholder="Enter reason..." onChange = {handleAddFormChange}/>
          <input type="number" name = "wedding_id" placeholder="Enter wedding id..." onChange = {handleAddFormChange}/>
          <input type="number" name = "amount" placeholder="Enter amount..." onChange = {handleAddFormChange}/>
          <button type="submit"> ADD </button>
        </form> 
      </div>                     
    </div>
  );

};
