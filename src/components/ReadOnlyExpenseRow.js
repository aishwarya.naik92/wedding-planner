import React from "react";

const ReadOnlyExpenseRow = ({ expense, handleEditClick, handleDeleteClick }) => {
  debugger;
  return (
    <tr>
      <td>{expense.expense_id}</td>
      <td>{expense.reason} </td>
      <td>{expense.wedding_id}</td>
      <td>{expense.amount}</td>
      <td>
        <button
          type="button"
          onClick={(event) => handleEditClick(event, expense)}
        >
          Edit
        </button>
        <button type="button" onClick={() => handleDeleteClick(expense.expense_id)}>
          Delete
        </button>
      </td>
    </tr>
  );
};

export default ReadOnlyExpenseRow;