import React, {useState} from "react";
import {render} from "react-dom";
import TextareaAutosize from "react-textarea-autosize";
import axios from 'axios';

export default function EncodeBase64FileUpload ()  {
    const [selectedFile, setSelectedFile] = useState([]);
    const [fileBase64String, setFileBase64String] = useState("");
    const [errorMessage, setErrorMessage] = useState("");

    const[imageUploaded, showImage] = useState("");

    const fileChanges = (e) =>
    {
        setSelectedFile(e.target.files);
    }

     const  encodeFiletoBase64 = (file) => {
        var reader = new FileReader();
        if(file) {
            reader.readAsDataURL(file);
            reader.onload  = () => {
                debugger;
                var Base64Result = reader.result.split(",");
                setFileBase64String(Base64Result[1]);
            }

            reader.onerror = function(error) {
                console.log('error: ' , error);
            }
        }

        if(selectedFile[0])
        {
            const [fileName, fileType] = selectedFile[0].name.split(".");
            const response =   axios.post( 'https://us-west2-wedding-planner-324818.cloudfunctions.net/uploadFile', {
                    name: fileName,
                    extension: fileType,
                    content: fileBase64String
                });  
        }
        
    }
    //encodeFiletoBase64(selectedFile[0]);

    return (
      <div>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "green",
            height: "60px",
            width: "100%",
            color: "#FFF",
            fontSize: "30px",
          }}
        >
        Files/Image Base64 Encoding
      </div>
      <br/>
      <input style={{
        justifyContent: "center",
        alignItems: "center",
        height: "60px",
        width: "60%",
        fontSize: "20px",
      }}
      type="file" id="input" onChange={fileChanges} />
      
      <TextareaAutosize
        style={{  
          height: "60px",
          width: "60%",
          fontSize: "20px",
        }}
        maxRows={20}
        value={fileBase64String}
        onChange={encodeFiletoBase64(selectedFile[0])}
      />
    </div>
    )
}