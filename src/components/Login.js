import React, { useState } from "react";
import axios from 'axios';
import {BrowserRouter as Router,  Redirect, Route, Link, Switch} from "react-router-dom";
import MainPage from './MainPage';
import logo from './wedding-couple_1.png'
export default function Login() {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [user, setUser] = useState("")
  const [errorMessage, setErrorMessage] = useState(null);

  function validateForm() {
    return email.length > 0 && password.length > 0;
  }

  function handleSubmit(event) {
    event.preventDefault();
  }

   async function validateData(event){
      const address = "http://wedding-planner-authorization-service.wedding-planner-324818.wl.r.appspot.com/employees/"+email+"/verify";
      const response = await axios.get(address);
      const obj = response.data;

      if(obj.error)
      { 
        setErrorMessage('User does not exist or Password incorrect, Please try again!');
      }
      else 
      {
        setUser(obj.fname);
      }
  }

  return (
    <div>
      <h1 style={{textAlign:"center"}}> Wedding Planner Service
        <img className= "logo" src={logo} alt="logo"/>
      </h1>
      <h2 style={{textAlign:"right"}}> We loved with a love that was more than love  </h2> 
      <div class="panel panel-info" >
      <div class="panel-heading">
        <div class="panel-title  col-md col-md-offset-3 column">
          Sign In
        </div>
        <div ></div>
      </div>   
      <Router>
      
      
      <div class="panel-body">
      {user? "": <form class="form-horizontal col-md-6 col-md-offset-3 column" onSubmit={handleSubmit}>
        <div class="input-group">
        <form class="form-horizontal" id="email">
          <label class="form-label" style={{padding:"20px;", margin: "10px" }}>Email:    </label>
          <input
            autoFocus
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </form>
        </div>

        <div  class="input-group">
        <form class="form-horizontal" id="password">
          <label class="form-label"  style={{padding:"20px;", margin: "10px"}}>Password:</label>
          <input
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </form>
        </div>
        <Link class="col-md-1 col-md-offset column" to={{pathname: "/weddingDisplay", user : user}} disabled={!validateForm()} onClick={validateData} >
          Login
        </Link>
          
        </form>}
        </div>
        <Route exact path="/weddingDisplay">
          <MainPage user = {user} errorMessage={errorMessage}></MainPage>
        </Route>
      </Router>
      </div>
    </div>
    
  );
}
