import React from "react";

const EditableRow = ({
  editFormData,
  handleEditFormChange,
  handleCancelClick,
}) => {
  return (
    <tr>
      <td>
        <input
          type="text"
          required="required"
          placeholder="Enter a wedding name"
          name="wedding_name"
          value={editFormData.wedding_name}
          onChange={handleEditFormChange}
        ></input>
      </td>
      <td>
        <input
          type="text"
          required="required"
          placeholder="Enter a wedding_description ..."
          name="wedding_description"
          value={editFormData.wedding_description}
          onChange={handleEditFormChange}
        ></input>
      </td>
      <td>
        <input
          type="text"
          required="required"
          placeholder="Enter a valid venueId"
          name="venue_id"
          value={editFormData.venue_id}
          onChange={handleEditFormChange}
        ></input>
      </td>
      <td>
        <input
          type="text"
          placeholder="Bride name"
          name="bride"
          value={editFormData.bride}
          onChange={handleEditFormChange}
        ></input>
      </td>
      <td>
        <input
          type="text"
          placeholder="Groom name"
          name="groom"
          value={editFormData.groom}
          onChange={handleEditFormChange}
        ></input>
      </td>
      <td>
        <input
          type="text"
          placeholder="Enter start time"
          name="start_time"
          value={editFormData.start_time}
          onChange={handleEditFormChange}
        ></input>
      </td>
      <td>
        <input
          type="text"
          placeholder="Enter endTime"
          name="end_time"
          value={editFormData.end_time}
          onChange={handleEditFormChange}
        ></input>
      </td>
      <td>
        <input
          type="number"
          placeholder="Enter budget planned"
          name="budget_planned"
          value={editFormData.budget_planned}
          onChange={handleEditFormChange}
        ></input>
      </td>
      <td>
        <input
          type="number"
          placeholder="Enter no of guests"
          name="no_of_guests"
          value={editFormData.no_of_guests}
          onChange={handleEditFormChange}
        ></input>
      </td>
      <td>
        <button type="submit">Save</button>
        <button type="button" onClick={handleCancelClick}>
          Cancel
        </button>
      </td>
    </tr>
  );
};

export default EditableRow;