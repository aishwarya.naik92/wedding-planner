import axios from 'axios';
import {useState} from 'react';
import {BrowserRouter as Router,  Link,   Route, Switch} from "react-router-dom";

import WeddingsDisplay from './WeddingsDisplay';
import EditableTable from './EditableTable';
import Login from './Login';
import EditExpenseEntry from './EditExpenseEntry';
import EncodeBase64FileUpload from './FileUpload';
import MessagingService from './MessagingService';

export default function MainPage(props){
    const [weddings, setWedding] = useState();

    async function getDisplayList(event)
    {
        const response = await axios.get(`http://35.202.158.179:3001/weddings`);
        const obj = response.data;
        setWedding(obj);
    }

    return (
        <div >
            <Router>
            {props.user ?  <h3>Welcome {props.user}</h3> : props.errorMessage}
            <ul class="nav nav-pills justify-content-end">
                {/* {(props.user)  ? "" : <li class="nav-item" style={{fontSize: '20px'}}>
                    <Link class="nav-link"  to={{pathname:"/login"}}>Login ASH</Link>
                </li>} */}
                {(props.user) ? <li class="nav-item" style={{fontSize: '20px'}}>
                    <Link class="nav-link" to ={{pathname:"/weddingsDisplay"}} onClick={getDisplayList}>Planner Display</Link>
                </li> : ""}

                <li class="nav-item" style={{fontSize: '20px'}}>
                    {(props.user)? <Link class="nav-link" to="/editEntry"> Planner: Edit Wedding Entries</Link> : ""}
                </li>

                <li class="nav-item" style={{fontSize: '20px'}}>
                    {(props.user)? <Link class="nav-link" to="/editExpenseEntry"> Planner: Edit Expense Entries</Link> : ""}
                </li>

                <li class="nav-item" style={{fontSize: '20px'}}>
                    {(props.user)? <Link class="nav-link" to="/messagingService"> Messaging Service</Link> : ""}
                </li>

                <li class="nav-item" style={{fontSize: '20px'}}>
                    {(props.user)? <Link class="nav-link" to="/fileUpload"> File upload</Link> : ""}
                </li>
            </ul>

            <Switch>

                <Route exact path="/weddingsDisplay">
                  {((weddings === undefined  ) )? "":<WeddingsDisplay weddingList={weddings} ></WeddingsDisplay>}
                </Route>
                <Route exact path="/editEntry">
                   <EditableTable></EditableTable>
                </Route>
                <Route exact path="/editExpenseEntry">
                   <EditExpenseEntry></EditExpenseEntry>
                </Route>
                <Route exact path="/fileUpload">
                  <EncodeBase64FileUpload></EncodeBase64FileUpload>
                </Route>
                <Route path="/messagingService">
                  <MessagingService userEmail={props.user}></MessagingService>
                </Route>
                {props.user ? "":<Route exact path="/login">
                    <Login></Login>
                </Route>}

            </Switch>
            </Router>
        </div>
    );
}
