import React, {useState, useEffect, Fragment} from "react";
const axios = require('axios')
import background from './table-background.jpeg'

export default function WeddingsDisplay(props){

    let weddingTable = props.weddingList === undefined? "" : props.weddingList.map((key) => {
            return  (
                <div>
                <table border = "black" class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Wedding Name</th>
                        <th scope="col">Wedding Description</th>
                        <th scope="col">Venue ID</th>
                        <th scope="col">Bride</th>
                        <th scope="col">Groom</th>
                        <th scope="col">Budget Planned</th>
                        <th scope="col">No of Guests</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">{key.wedding_id}</th>
                        <td class="table-secondary">{key.wedding_name}</td>
                        <td class="table-success">{key.wedding_description}</td>
                        <td class="table-info" >{key.venue_id}</td>
                        <td class = "table-primary">{key.bride}</td>
                        <td class="table-secondary" >{key.groom}</td>
                        <td>{key.budget_planned}</td>
                        <td>{key.no_of_guests}</td>
                    </tr>
                </tbody>
                </table>
    
            </div>
            )
    });


    return  ( 
        <div>
        {
            (props.weddingList !== undefined)? <div> {weddingTable}</div> : "" 
        }
        </div>
    );
}
