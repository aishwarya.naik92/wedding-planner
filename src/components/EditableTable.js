import React, {useState, useEffect, Fragment} from "react";
import ReadOnlyRow from "./ReadOnlyRow";
import EditableRow from "./EditableRow";
import '../../src/editableTable.css'

const axios = require('axios')

export default function EditableTable() {

    const [weddingData, setWeddingData]  = useState([]);
    const [editWeddingId, setEditWeddingId] = useState(null);

      useEffect(async () => {
        const result = await axios(
          'http://35.202.158.179:3001/weddings',
        );
     
        setWeddingData(result.data);
      }, []);

    const [addFormData, setAddFormData] = useState({
        wedding_name : '',
        wedding_description: '',
        venue_id: '',
        bride: '',
        groom: '',
        start_time: '',
        end_time: '',
        budget_planned: '',
        no_of_guests: ''
    });

    const [editFormData, setEditFormData] = useState({
        wedding_name : '',
        wedding_description: '',
        venue_id: '',
        bride: '',
        groom: '',
        start_time: '',
        end_time: '',
        budget_planned: '',
        no_of_guests: ''
      });


  const handleAddFormChange = (event) => {
      event.preventDefault();

      const fieldName  = event.target.getAttribute('name');
      const fieldValue = event.target.value;

      const newFormData = {...addFormData};
      newFormData[fieldName] = fieldValue;

      setAddFormData(newFormData);
  }

  const handleEditFormChange = (event) => {
    event.preventDefault();

    const fieldName = event.target.getAttribute("name");
    const fieldValue = event.target.value;

    const newFormData = { ...editFormData };
    newFormData[fieldName] = fieldValue;
    setEditFormData(newFormData);
  };

  const handleAddFormSubmit = async (event) => {
      event.preventDefault();

      const newWedding = {
          wedding_name: addFormData.wedding_name,
          wedding_description : addFormData.wedding_description,
          venue_id : addFormData.venue_id,
          bride: addFormData.bride,
          groom: addFormData.groom,
          start_time: addFormData.start_time,
          end_time: addFormData.end_time,
          budget_planned: addFormData.budget_planned,
          no_of_guests: addFormData.no_of_guests

      };

      const newWeddingData = [...weddingData, newWedding];
      setWeddingData(newWeddingData);
      await axios.post('http://35.202.158.179:3001/weddings', newWedding);
      setEditWeddingId(null);
  }

  

  const handleEditFormSubmit = async (event) => {
    event.preventDefault();
    debugger

    const editedWedding = {
      wedding_id: editWeddingId,
      wedding_name: editFormData.wedding_name,
      wedding_description: editFormData.wedding_description,
      venue_id: editFormData.venue_id,
      bride: editFormData.bride,
      groom: editFormData.groom,
      start_time: editFormData.start_time,
      end_time: editFormData.end_time,
      budget_planned: editFormData.budget_planned,
      no_of_guests: editFormData.no_of_guests
    };

    const newWeddings = [...weddingData];

    const index = weddingData.findIndex((wedding) => wedding.wedding_id === editWeddingId);

    newWeddings[index] = editedWedding;

    setWeddingData(newWeddings);

    const response = axios.put(`http://35.202.158.179:3001/weddings/${editedWedding.wedding_id}`, editedWedding);
    //error
    setEditWeddingId(null);
  };

  const handleEditClick = (event, wedding) => {
    event.preventDefault();
    setEditWeddingId(wedding.wedding_id);

    const formValues = {
      wedding_name: wedding.wedding_name,
      wedding_description: wedding.wedding_description,
      venue_id: wedding.venue_id,
      bride: wedding.wedding_id,
      groom: wedding.wedding_id,
      start_time: wedding.start_time,
      end_time: wedding.end_time,
      budget_planned: wedding.budget_planned,
      no_of_guests: wedding.no_of_guests
    };

    setEditFormData(formValues);
  };

  const handleCancelClick = () => {
    setEditWeddingId(null);
  };

  const handleDeleteClick = async (wedding_id) => {
    const newWeddings = [...weddingData];
    const index = weddingData.findIndex((wedding) => wedding.wedding_id === wedding_id);
    newWeddings.splice(index, 1);
    setWeddingData(newWeddings);
    await axios.delete(`http://35.202.158.179:3001/weddings/${wedding_id}` );
  };

    
    return (
        
        
        <div>
            <form onSubmit = {handleEditFormSubmit}>
            <table>
                <thead>
                    <tr>
                        <th> Wedding_id </th>
                        <th>Wedding Name</th>
                        <th>Description</th>
                        <th>Venue_id</th>
                        <th>Bride</th>
                        <th>Groom</th>
                        <th>Event Start Time</th>
                        <th>Event End Time</th>
                        <th>Budget Planned</th>
                        <th>No Of Guests</th>
                    </tr>
                </thead>
                <tbody>
                        {weddingData.map((wedding) => (
                            <Fragment>
                                {editWeddingId === wedding.wedding_id ? (
                                <EditableRow
                                    editFormData={editFormData}
                                    handleEditFormChange={handleEditFormChange}
                                    handleCancelClick={handleCancelClick}
                                />
                                ) : (
                                <ReadOnlyRow
                                    wedding={wedding}
                                    handleEditClick={handleEditClick}
                                    handleDeleteClick={handleDeleteClick}
                                />
                                )}
                            </Fragment>
                        ))}

                </tbody>
            </table>
            </form>
            <form onSubmit={handleAddFormSubmit}>
            <h2>Add a new Wedding Row</h2>
            
                <input type="text" name = "wedding_name" placeholder="Enter wedding name..." onChange = {handleAddFormChange}/>
                <input type="text" name = "wedding_description" placeholder="Enter wedding description..." onChange = {handleAddFormChange}/>
                <input type="text" name = "venue_id" placeholder="Enter venue id..." onChange = {handleAddFormChange}/>
                <input type="text" name = "bride" placeholder="Enter bride name..." onChange = {handleAddFormChange}/>
                <input type="text" name = "groom" placeholder="Enter groom name..." onChange = {handleAddFormChange}/>
                <input type="text" name = "start_time" placeholder="Enter wedding start_time..." onChange = {handleAddFormChange}/>
                <input type="text" name = "end_time" placeholder="Enter wedding end time..." onChange = {handleAddFormChange}/>
                <input type="number" name = "budget_planned" placeholder="Enter budget planned..." onChange = {handleAddFormChange}/>
                <input type="number" name = "no_of_guests" placeholder="Enter no of guests..." onChange = {handleAddFormChange}/>
                <button type="submit"> ADD </button>

            </form>                        
        </div>
    )
};
